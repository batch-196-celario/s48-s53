import {useState} from 'react';
import { Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard ({courseProp}) {
	//console.log(props)
	//console.log(typeof props);

	//Object destructuring
	const {name, description, price, _id} = courseProp

	// const [count, setCount] = useState(0);
	// const [seat, setSeats] = useState(10)
	// //console.log(useState(0));

	// function enroll(){
		
	// 	console.log(`Enrollees: ${count}`);
	// 	if (seat === 0){
	// 		alert("No more seats available, check back later");
	// 	} else {
	// 		setCount(count + 1);
	// 		setSeats(seat - 1);
	// 	}
	// };


	return(
				<Card className="p-3 mb-3">
					<Card.Body>
						<Card.Title className="fw-bold">
							{name}
						</Card.Title>
						<Card.Subtitle>Course Description:</Card.Subtitle>
						<Card.Text>
							{description}
						</Card.Text>
					    <Card.Subtitle>Course Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						{/*<Card.Text>Enrollees: {count}</Card.Text>
						<Card.Text>Seats: {seat}</Card.Text>*/}
						{/*<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
						<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>						
					</Card.Body>
				</Card>
	)
}