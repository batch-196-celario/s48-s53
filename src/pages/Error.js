import {Link} from 'react-router-dom';
import {Row, Col, Button} from 'react-bootstrap';




export default function Error (){
	return (
		<Row>
			<Col className = "p-5">
				<h1>404 page not found</h1>
				<p>the page your are looking for cannot be found</p>
				<Button variant="primary" as={Link} to="/">back to home</Button>
			</Col>
		</Row>
	)
}