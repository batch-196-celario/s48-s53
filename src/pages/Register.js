import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register (){

	const {user, setUser} = useContext(UserContext);

	const history = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("")
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	//const [password2, setPassword2] = useState("");
 	const [isActive, setIsActive] = useState(false) 

 	//Check if values are successfully binded
 	// console.log(email);
 	// console.log(password1);
 	// console.log(password2);

 	function registerUser(e){
 		e.preventDefault();

 		fetch('http://localhost:4000/users/checkEmailExists', {
 			method: 'POST',
 			headers: {
 				'Content-Type' : 'application/json'
 			},
 			body: JSON.stringify({
 				email: email
 			})
 		})
 		.then(res => res.json())
 		.then(data => {
 			console.log(data)

 			if(data){
 				Swal.fire({
 					title: "Duplicate email found",
 					icon: "info",
 					text: "The email that you're trying to register already exist"
 				});
 			} else {
 				fetch('http://localhost:4000/users', {
 					method: 'POST',
 					headers: {
 						'Content-Type' : 'application/json'
 					},
 					body: JSON.stringify({
 						firstName: firstName,
 						lastName: lastName,
 						mobileNo: mobileNo,
 						email: email,
 						password: password
 					})
 				})
 			.then(res => res.json())
 			.then(data => {
 				console.log(data)

 				if(data.email){
 					Swal.fire({
 						title: "Registration successful!",
 						icon: "success",
 						text: "Thank you for registering"
 					})
 					history("/login");
 				} else {
 					Swal.fire({
 						title: "Registration Failed",
 						icon: "error",
 						text: "Something went wrong, try again"
 					})
 				}
 			})
 			};
 		})

 		setEmail("");
 		setPassword("");
 		setFirstName("");
 		setLastName("");
 		setMobileNo("");
 		// alert('Thank you for registering')
 	}

 	//Syntax: useEffect(() => {}, [])
 	useEffect(() => {
 		if(email !== "" && firstName !== "" && lastName !== "" && password !== "" && mobileNo !== "" && mobileNo.length === 11){
 			setIsActive(true);
 		} else {
 			setIsActive(false)
 		}

 	}, [email, firstName, lastName, mobileNo, password]);

	return (
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Register Here:</h1>
		<Form onSubmit ={e => registerUser(e)}>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type= "text"
					placeholder = "Enter your First Name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type= "text"
					placeholder = "Enter your Last Name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile No.</Form.Label>
				<Form.Control
					type= "text"
					placeholder = "Enter your Mobile Number"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>


			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "Enter your email here"
					required
					value = {email}
					onChange= {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll Never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password"
					required
					value = {password}
					onChange = {e => setPassword(e.target.value)}
				/>	
			</Form.Group>

		{ isActive ?
			<Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
				Register
			</Button>
			:
			<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
				Register
			</Button>
		}
		</Form>
		</>

	)

}