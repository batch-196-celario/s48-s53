import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	const {user, setUser} = useContext(UserContext);
	//console.log(user);
	
	const [loginEmail, setLogInEmail] = useState("");
	const [password, setPassword] = useState("");
    const [active, setActive] = useState(false)

    function loginUser(e){
 		e.preventDefault();

 		fetch('http://localhost:4000/users/login', {
 			method: 'POST',
 			headers: {
 				'Content-Type' : 'application/json'
 			},
 			body: JSON.stringify({
 				email: loginEmail,
 				password: password
 			})
 		})
 		.then(res => res.json())
 		.then(data => {
 			console.log(data);

 			if(typeof data.accessToken !== "undefined"){
 				localStorage.setItem('token', data.accessToken)
 				retrieveUserDetails(data.accessToken);
 				
 				Swal.fire({
 					title: "Login Succesful",
 					icon: "success",
 					text: "Welcome to Booking App of 196"
 				});
 			} else {
 				Swal.fire({
 					title: "Authetication Failed",
 					icon: "error",
 					text: "Check your credentials"
 				});
 			};
 		});

 		// localStorage.setItem("email", loginEmail)

 		// setUser({
 		// 	email: localStorage.getItem('email')
 		// });


 		setLogInEmail("");
 		setPassword("");

 		//alert(`${loginEmail} Login succesful`)
 	};

 	const retrieveUserDetails = (token) => {
 		fetch('http://localhost:4000/users/getUserDetails', {
 			headers: {
 				Authorization: `Bearer ${token}`
 			}
 		})
 		.then(res => res.json())
 		.then(data => {
 			console.log(data);

 			setUser({
 				id: data._id,
 				isAdmin: data.isAdmin
 			});
 		})
 	}

    useEffect(() => {
 		if(loginEmail !== "" && password !== ""){
 			setActive(true);
 		} else {
 			setActive(false)
 		}

 	}, [loginEmail,password]);

	return (
		 (user.id !== null)?
		 	<Navigate to="/courses"/>
		 :
		<>
		<h1>Login</h1>
		<Form onSubmit ={e => loginUser(e)}>
			<Form.Group controlId="logInEmail">
				<Form.Label>
					Email:
				</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "Enter your email"
					required
					value = {loginEmail}
					onChange= {e => setLogInEmail(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="logInPassword">
				<Form.Label>
					Password:
				</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password"
					required
					value = {password}
					onChange = {e => setPassword(e.target.value)}
				/>
			</Form.Group>

			<p className='p-1'>Not yet registered? <Link to='/register'>Register Here</Link></p>

		{ active ?
			<Button variant="success" type="submit" id="logInBtn" className="mt-3 mb-5">
				Login
			</Button>
			:
			<Button variant="danger" type="submit" id="logInBtn" className="mt-3 mb-5" disabled>
				Login
			</Button>
		}				
		</Form>
		</>
	)
}