


const coursesData = [
	{	
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Learn PHP laravel while you are at home",
		price: "45000",
		onOffer: true
	},
	{	
		id: "wdc002",
		name: "Python-Django",
		description: "Learn Python while you are at home",
		price: "55000",
		onOffer: true
	},
	{	
		id: "wdc003",
		name: "Java-Springboot",
		description: "Learn Java while you are at home",
		price: "55000",
		onOffer: true
	}

]

export default coursesData;